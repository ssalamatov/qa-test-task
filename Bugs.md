## Bugs

1 Right-open interval

Right argument does not include in the interval: [interval_start, interval_end) 

Example:
input: get_sum(1, 10)
expected: 385
actual: 285

2 Negative value does not support

Left and right arguments in the interval can be both: positive and negative. Using negative
 argument leads to OverflowError exception

Example:
input: get_sum(-1, 10)
expected: 385
actual: OverflowError

3 Reverse interval

If start more than end argument in the interval, it means that interval is incorrectly composed and
    should be lead to custom exception.

Example:
input: get_sum(10, 1)
expected: Exception
actual: 0

4 BigInt values in interval

Using big ints in interval lead to overflow errors

Example:
input: get_sum(10000000000000000000, 10000000000000000001)
expected: 200000000000000000020000000000000000001
actual: OverflowError
