import sys

# add here path to testlib folder
sys.path.append("..")

from testlib import SquaresAdder

help(SquaresAdder)

res = SquaresAdder.get_sum(1, 10)

print(res)
