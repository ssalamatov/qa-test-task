FROM python:3-alpine
ENV PYTHONUNBUFFERED 1
RUN mkdir /src
WORKDIR /src
RUN apk add --no-cache libstdc++
RUN pip3 install pytest
COPY . /src/
RUN ["pytest", "-v", "separate.py"]
