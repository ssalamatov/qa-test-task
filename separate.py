# -*- coding: utf-8 -*-

import pytest

from testlib import SquaresAdder


def calc_sum(start: int, end: int) -> int:
    """ calc the sum of squared numbers from the interval inclusively """

    x = 0
    for i in range(start, end+1):
        x += (i ** 2)
    return x


class TestSquaresAdder:

    @pytest.fixture
    def test_get_sum(self):
        return SquaresAdder.get_sum

    @pytest.mark.parametrize('start, end', [
        (1, 10),
        (0, 1),
        (0, 0),
        (10, 10),
        (-10, 10),
    ])
    def test_sum(self, test_get_sum, start, end):
        assert calc_sum(start, end) == test_get_sum(start, end)

    @pytest.mark.parametrize('start, end', [
        ('a', 10),
        (1, 'a'),
        ('a', 'b'),
        (1.05, 10),
        (1, 10.1),
        (1.1, 10.1),
    ])
    def test_invalid_types(self, test_get_sum, start, end):
        """ start or end of interval can only be integers """

        with pytest.raises(
                Exception,
                match='^Python argument types'
        ):
            test_get_sum(start, end)

    @pytest.mark.parametrize('start, end', [
        (10, 1),
    ])
    def test_incorrect_interval(self, test_get_sum, start, end):
        """ start should be less or equal than end """

        with pytest.raises(
                Exception,
        ):
            test_get_sum(start, end)

    @pytest.mark.parametrize('start, end', [
        (10000000000000000000, 10000000000000000001),
    ])
    def test_big_int(self, test_get_sum, start, end):
        assert calc_sum(start, end) == test_get_sum(start, end)
